Gonzalo's zsh theme
-------------------

Minimalistic theme.

It will only display the current directory name between brackets,
and the hostname if it detects a SSH connection.
