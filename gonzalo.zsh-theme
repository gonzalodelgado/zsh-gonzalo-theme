# Based on pmcgee and wedisagree

if [ $UID -eq 0 ]; then NCOLOR="blue"; else NCOLOR="green"; fi

if [ -n "${SSH_CONNECTION+1}" ];
then PR_HOST='@%m '
else PR_HOST=''
fi

PROMPT='%{$fg_bold[green]%}${PR_HOST}%{$fg_bold[$NCOLOR]%}[%c] %{$reset_color%}'
RPROMPT='$(git_prompt_info)%{$reset_color%}$(git_prompt_status)%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[yellow]%} *"
